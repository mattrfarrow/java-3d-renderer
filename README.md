A simple 3d renderer that I wrote because I found myself wondering how 3d rendering software works and thought I'd see if my ideas were remotely right.

It largely works!  There are some glitches in the rendering if you get close to objects but it has largely served its purpose for now.

I used Java for this because I wanted to try Java 14's record syntax - case classes are one my most missed features from Scala.